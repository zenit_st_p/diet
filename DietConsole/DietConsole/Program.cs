﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Diet;
using Logic;

namespace DietConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Sallet sallet = SalletFactory.CreateSallet();

            SalletPrinter printer = new SalletPrinter();
            printer.Print(sallet);

            SalletCalculator calculator = new SalletCalculator();
            int calorific = calculator.CalculateCalorific(sallet);

            Console.WriteLine("Total sallet calorific " + calorific);
            Console.ReadKey();
        }
    }
}
