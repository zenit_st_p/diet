﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Diet;
using Logic;

namespace DietConsole
{
    public class SalletPrinter
    {
        public void Print(Sallet sallet)
        {
            List<AbstractVegetable> vegs = sallet.Vegs;

            foreach (AbstractVegetable veg in vegs)
            {
                Console.WriteLine("Name: " + veg.Name);
                Console.WriteLine("Calorific: " + veg.Calorific_value);
                Console.WriteLine();
            }
        }
    }
}
