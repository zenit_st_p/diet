﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Diet;

namespace Logic
{
    public class SalletFactory
    {
        public static Sallet CreateSallet()
        {
            Solanales solanales = new Solanales();
            solanales.Name = "Tomato";
            solanales.Structure_sprout = "straight";
            solanales.Calorific_value = 500;

            Cucurbitales cucurbitales = new Cucurbitales();
            cucurbitales.Name = "Pumpkin";
            cucurbitales.Dioecious_flowers = true;
            cucurbitales.Calorific_value = 300;

            Sallet sallet = new Sallet();
            sallet.AddVeg(cucurbitales);
            sallet.AddVeg(solanales);

            return sallet;
        }
    }
}
