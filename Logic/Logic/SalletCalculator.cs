﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Diet;

namespace Logic
{
    public class SalletCalculator
    {
        public int CalculateCalorific(Sallet sallet)
        {
            int calorific = 0;

            List<AbstractVegetable> vegs = sallet.Vegs;

            foreach (AbstractVegetable veg in vegs)
            {
                calorific += veg.Calorific_value;
            }
            return calorific;
        }
    }
}
