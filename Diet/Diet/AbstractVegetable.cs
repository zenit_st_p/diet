﻿using System;
public abstract class AbstractVegetable
{
    private string name;
    private int calorific_value;

    public string Name
    {
        get { return name; }
        set { name = value; }
    }

    public int Calorific_value
    {
        get { return calorific_value; }
        set { calorific_value = value; }
    }
}
