﻿using Diet;
public class Cucurbitales : AbstractVegetable
{
    private bool dioecious_flowers;

    public bool Dioecious_flowers
    {
        get { return dioecious_flowers; }
        set { dioecious_flowers = value; }
    }
}
