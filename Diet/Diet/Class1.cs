﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diet
{
    public class Sallet
    {
        public List<AbstractVegetable> vegs;

        public void AddVeg(AbstractVegetable veg)
        {
            if (vegs == null)
            {
                vegs = new List<AbstractVegetable>();

            }

            vegs.Add(veg);
        }

        public List<AbstractVegetable> Vegs
        {
            get { return vegs; }
        }
    }
}
